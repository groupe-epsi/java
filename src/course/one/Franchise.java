package course.one;

import java.util.Scanner;

/**
 * <p>Une compagnie d’assurance effectue des remboursements en ponctionnant une franchise de 20%.
 * <p>La franchise ne peut execder 200 €.
 * <p>
 * <p>Réaliser un programme demandant à l’utilisateur de saisir le montant des dommages et calculer la
 * valeur de la franchise et le montant du remboursement.
 */
public class Franchise {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Veuillez saisir le montant des dommages : ");
        int damageAmount = scanner.nextInt() * 100;

        int franchise = Math.min((int) (damageAmount * 0.2), 20_000);

        System.out.printf("Votre franchise est de %.2f %n", franchise / 100f);
        System.out.printf("Le montant du remboursement est de %.2f %n", (damageAmount - franchise) / 100f);
    }
}
