package course.one;

import java.util.Scanner;

/**
 * Exercice 2 :
 * <p>Ecrire un programme demandant l’utilisateur son nom puis affichant le nom saisi.
 * <p>Compléter ce programme en lui demandant son age au format numérique.
 * <p>En sortie, afficher s’il est majeur ou mineur.
 */
public class UserInfo {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Veuillez entrer votre nom");
        String name = scanner.next();
        System.out.println("Votre nom est: " + name);

        System.out.println("Veuillez entrer votre âge (format numérique)");
        int age = scanner.nextInt();
        String majority = age >= 18 ? "Majeur" : "Mineur";
        System.out.printf("Votre âge est : %s, et vous êtes %s", age, majority);
    }
}
