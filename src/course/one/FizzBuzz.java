package course.one;

/**
 * Écrire un programme qui affiche les nombres de 1 à 199.
 * <p>Mais pour les multiples de 3, afficher “Fizz” au lieu du nombre et pour les multiples de 5 afficher “Buzz”.
 * <p>Pour les nombres multiples de 3 et 5, afficher “FizzBuzz”.
 */
public class FizzBuzz {

    public static void main(String[] args) {
        for (int i = 1; i <= 199; i++) {
            if (i % 3 == 0 && i % 5 == 0) System.out.print(" FizzBuzz ");
            else if (i % 5 == 0) System.out.print(" Buzz ");
            else if (i % 3 == 0) System.out.print(" Fizz ");
            else System.out.printf(" %s ", i);
        }
    }
}
