package course.one;

import java.util.Scanner;

public class DisplaySquare {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Saisisez un nombre");
        int width = scanner.nextInt();

        for (int y = 0; y < width; y++) {
            for (int x = 0; x < width; x++) {
                System.out.print("#");
            }
            System.out.println();
        }
    }
}
