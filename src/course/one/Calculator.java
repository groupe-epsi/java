package course.one;

import java.util.Scanner;

/**
 * Ecrire un programme demandant de :
 * <ul>
 *     <li>saisir 2 valeurs assignées aux a et b</li>
 *     <li>saisir un opérateur parmi + - / * </li>
 * </ul>
 * En utilisant un <strong>switch</strong>, réaliser l’opération et afficher le résultat.
 */
public class Calculator {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Entrez votre opération: ");
        String[] calculation = scanner.nextLine().split(" ");

        int firstOperand = Integer.parseInt(calculation[0]);
        char operation = calculation[1].charAt(0);
        int secondOperand = Integer.parseInt(calculation[2]);

        int result = switch (operation) {
            case '+' -> firstOperand + secondOperand;
            case '/' -> firstOperand / secondOperand;
            case '-' -> firstOperand - secondOperand;
            case '*' -> firstOperand * secondOperand;
            default -> throw new IllegalArgumentException("Operation inconnue");
        };

        System.out.printf("%s %s %s = %s", firstOperand, operation, secondOperand, result);
    }
}
