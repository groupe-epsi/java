package course.one;

import java.util.Random;
import java.util.Scanner;

public class Sum {
    public static final int ARRAY_SIZE = 10;

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int[] numbers = new int[ARRAY_SIZE];
        for (int i = 0; i < numbers.length; i++) {
            numbers[i] = i + 1;
        }

        int sum = sum(numbers);
        int expectation = (ARRAY_SIZE * (ARRAY_SIZE + 1) / 2);
        if (sum == expectation) {
            System.out.println("Working");
        } else {
            System.out.println("Not working");
        }

        System.out.print("Veuillez saisir un nombre à trouver");
        int numberToFind = scanner.nextInt();

        Random randomNumberGenerator = new Random();
        int[] randomNumbers = new int[ARRAY_SIZE];

        int foundIndex = -1;
        for (int i = 0; i < randomNumbers.length; i++) {
            randomNumbers[i] = randomNumberGenerator.nextInt(10);
            if (randomNumbers[i] == numberToFind) {
                foundIndex = i;
            }
        }

        if (foundIndex > 0) {
            System.out.printf("Valeur trouvé : %d", foundIndex);
        } else {
            System.out.println("Valeur non trouvé");
        }
    }

    private static int sum(int[] array) {
        int result = 0;
        for (int value : array) {
            result += value;
        }
        return result;
    }
}
