package course.one;

import java.util.Scanner;

/**
 * Exercice 3
 * <p>Créer un programme permettant de saisir :
 * <ul>
 *  <li>un montant hors taxe</li>
 *  <li>la taxe à appliquer</li>
 * </ul>
 * <p>Puis de calculer le montant TTC et afficher le resultat.
 */
public class Taxes {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Saissez le montant HT : ");
        float amount = scanner.nextFloat();

        System.out.print("Saisissez la valeur de la taxe à appliquer en pourcentage : ");
        float taxRate = scanner.nextFloat();

        int total = (int) (amount * (100 + taxRate));
        System.out.printf("Montant TTC : %.2f", total / 100f);
    }
}
