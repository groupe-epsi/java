package course.two;

/**
 * Je vous propose d’implementer le jeu du look and say. Le principe consiste à compter le nombre de fois qu’apparait un chiffre et de le dire à haute voix (ici de l’ecrire)
 * <p>
 * Par exemple, 211 devient 1221 (1 2, 2 1).
 * <p>
 * Par exemple :
 * <ul>
 *     <li>1 devient 11 (1 1)</li>
 *     <li>11 deveient 21 (2 1)</li>
 *     <li>21 devient 1211</li>
 *     <li>1211 devient 111221</li>
 *     <li>111221 devient 312211</li>
 *     <li>1 devient 11 (1 1)</li>
 *     <li>1 devient 11 (1 1)</li>
 * </ul>
 * Répeter 40 fois la lecture en partant de 1113222113. Combien de caractères composent le dernier nombre ?
 */
public class LookAndSay {

    public static void main(String[] args) {
        StringBuilder numberToSay = new StringBuilder("1113222113");
        for (int i = 0; i < 40; i++) {
            String result = getNumberToSay(numberToSay.toString());
            numberToSay.setLength(0);
            numberToSay.append(result);
        }
        System.out.printf("Résultat final : %d charactères%n", numberToSay.length());
    }

    private static String getNumberToSay(String number) {
        StringBuilder numberToSay = new StringBuilder();

        int lastDigitOccurrence = 1;
        char lastDigit = number.charAt(0);
        for (int i = 1; i < number.length(); i++) {
            char currentDigit = number.charAt(i);

            if (lastDigit != currentDigit) {
                numberToSay.append(lastDigitOccurrence).append(lastDigit);
                lastDigitOccurrence = 0;
                lastDigit = currentDigit;
            }

            lastDigitOccurrence++;
        }

        numberToSay.append(lastDigitOccurrence).append(lastDigit);
        return numberToSay.toString();
    }
}
