package course.three.monstertruck;

/**
 * Ecrire une classe MonsterTruck ayant le comportement ci-dessous.
 * <p>Vous devez utiliser la méthode System.out quand vous ne voyez aucune autre possibilité.
 * <p>m1: monster 1
 * <p>m2: truck 1 <p>car 1
 * <p>toString: monster vroomvroom
 */
public class Main {

    public static void main(String[] args) {
        MonsterTruck monsterTruck = new MonsterTruck();
        System.out.println("m1:");
        monsterTruck.m1();
        System.out.println();
        System.out.println("m2:");
        monsterTruck.m2();
        System.out.println();
        System.out.println("tostring:");
        System.out.println(monsterTruck.toString());
    }
}
