package course.three.gameoflife;

public class AliveCell implements Cell {

    @Override
    public Cell play(int neighbourCount) {
        return (neighbourCount == 2 || neighbourCount == 3) ? this : new DeadCell();
    }

    @Override
    public boolean isAlive() {
        return true;
    }

    @Override
    public char getCellView() {
        return '+';
    }
}
