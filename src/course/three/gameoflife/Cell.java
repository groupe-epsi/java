package course.three.gameoflife;

public interface Cell {

    /**
     * Indique si la cellule est en vie
     *
     * @return true si en vie, false sinon
     */
    boolean isAlive();

    /**
     * Donne une representation de la cellule
     *
     * @return “+” pour en vie, “-“ pour mort
     */
    char getCellView();

    /**
     * Retourne une nouvelle cellule en fonction du nombre de voisins en vie
     *
     * @param neighbourCount Le nombre de voisin de la cellule
     * @return Une cellule en fonction du nombre de voisin
     */
    Cell play(int neighbourCount);
}
