package course.three.gameoflife;

public class DeadCell implements Cell {

    @Override
    public Cell play(int neighbourCount) {
        return neighbourCount == 3 ? new AliveCell() : this;
    }

    @Override
    public boolean isAlive() {
        return false;
    }

    @Override
    public char getCellView() {
        return '-';
    }
}
