package course.three.gameoflife;

/**
 * Le jeu de la vie est un automate cellulaire imaginé par John Horton Conway
 * en 1970 et qui est probablement le plus connu de tous les automates cellulaires.
 * <p>Malgré des règles très simples, le jeu de la vie est Turing-complet.
 * <p><a href>https://www.youtube.com/watch?v=E8kUJL04ELA</a>
 */
public class Main {

    public static void main(String[] args) {
        Game game = new Game(6, 6);
        game.display();

        System.out.println();
        game.play(1);
        game.display();

        System.out.println();
        game.play(1);
        game.display();

        System.out.println();
        game.play(1);
        game.display();
    }
}
