package course.three.gameoflife;

/**
 * Implémentation du jeu de la vie
 */
public class Game {
    // Tableau temporaire pour effectuer des changements de cellules sans affecter le compte des voisins
    private Cell[][] tmpCells;
    private Cell[][] cells;

    /**
     * Construit un jeu de la vie avec un nombre spécifié de lignes et colonnes
     *
     * @param rows Le nombre de lignes
     * @param columns Le nombre de colonnes
     */
    public Game(int rows, int columns) {
        // On ajouter une ligne/colonne dans chaque coté pour ne pas avoir
        // à checker les bords dans le comptage des voisins
        tmpCells = new Cell[rows + 2][columns + 2];
        cells = new Cell[rows + 2][columns + 2];

        // Generation des cellules
        for (int y = 0; y < cells.length; y++) {
            for (int x = 0; x < cells[y].length; x++) {
                // On ne génère pas de cellules pour les bords
                if (x == 0 || x == cells[y].length - 1 || y == 0 || y == cells.length - 1) {
                    cells[y][x] = new DeadCell();
                } else {
                    cells[y][x] = Math.random() > 0.75 ? new AliveCell() : new DeadCell();
                }
                tmpCells[y][x] = cells[y][x];
            }
        }
    }

    /**
     * Affiche le tableau de cellules
     */
    public void display() {
        for (int y = 1; y < cells.length - 1; y++) {
            for (int x = 1; x < cells[y].length - 1; x++) {
                System.out.print(cells[y][x].getCellView());
            }
            System.out.println();
        }
    }

    /**
     * Génére le nombre de cycles indiqué.
     *
     * @param step Le nombre de cycles à éxécuté
     */
    public void play(int step) {
        for (int i = 0; i < step; i++) {
            play();
            swap();
        }
    }

    /**
     * Génére un cycle
     * @see Game#play(int) Méthode pour générer un nombre détérminé de cycle
     */
    private void play() {
        for (int y = 1; y < tmpCells.length - 1; y++) {
            for (int x = 1; x < tmpCells[y].length - 1; x++) {
                tmpCells[y][x] = cells[y][x].play(getCellNeighborCount(x, y));
            }
        }
    }

    /**
     * Swap le tableau temporaire avec le tableau principal
     */
    private void swap() {
        Cell[][] tmp = this.tmpCells;
        this.tmpCells = this.cells;
        this.cells = tmp;
    }

    /**
     * Retourne le nombre de cellules voisine d'une cellule cible
     * @param x L'indice en x de la célulle
     * @param y l'indice en y de la céllule
     * @return Le nombre de cellules voisine
     */
    private int getCellNeighborCount(int x, int y) {
        // On ne tient pas en compte la cellule cible si elle est vivante
        int neighborCount = cells[y][x] instanceof AliveCell ? -1 : 0;

        // On compte le nombre de cellule vivante
        for (int neighbourY = y - 1; neighbourY <= y + 1; neighbourY++) {
            for (int neighbourX = x - 1; neighbourX <= x + 1; neighbourX++) {
                if (cells[neighbourY][neighbourX] instanceof AliveCell) {
                    neighborCount++;
                }
            }
        }

        return neighborCount;
    }
}
