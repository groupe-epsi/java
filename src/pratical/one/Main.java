package pratical.one;

import java.util.*;

public class Main {

    /**
     * Method to count characters occurrences in a word
     *
     * @param word The word to count characters occurrences on
     * @return A character's occurrences map
     */
    private static Map<Character, Integer> countCharactersOccurrences(String word) {
        Map<Character, Integer> occurrences = new HashMap<>();
        for (int i = 0; i < word.length(); i++) {
            char c = word.charAt(i);
            occurrences.put(c, occurrences.getOrDefault(c, 0) + 1);
        }
        return occurrences;
    }

    /**
     * Find the most used character in a word
     *
     * @param word The word to find the most used character on
     * @return The most used character
     */
    public static char findMostUsedCharacter(String word) {
        Map<Character, Integer> occurrences = countCharactersOccurrences(word);

        return occurrences.entrySet().stream()
                .max(Comparator.comparingInt(Map.Entry::getValue))
                .orElseThrow()
                .getKey();
    }

    /**
     * Check if two words are anagrams
     *
     * @param firstWord  A first word
     * @param secondWord A second word
     * @return True if the two words are anagrams, false otherwise
     */
    public static boolean isAnagram(String firstWord, String secondWord) {
        if (firstWord.length() != secondWord.length()) return false;

        Map<Character, Integer> firstWordOccurrences = countCharactersOccurrences(firstWord);
        Map<Character, Integer> secondWordOccurrences = countCharactersOccurrences(secondWord);

        return firstWordOccurrences.equals(secondWordOccurrences);
    }

    /**
     * Dispense cash in a way to dispense as little bills as possible
     *
     * @param cashToDispense The cash to dispense
     */
    public static void dispenseCash(int cashToDispense) {
        int fiftyBills = cashToDispense / 50;
        cashToDispense -= fiftyBills * 50;
        System.out.println("Sum of fifty bills : " + fiftyBills);

        int twentyBills = cashToDispense / 20;
        cashToDispense -= twentyBills * 20;
        System.out.println("Sum of twenty bills : " + twentyBills);

        int tenBills = cashToDispense / 10;
        cashToDispense -= tenBills * 10;
        System.out.println("Sum of ten bills : " + tenBills);

        int fiveBills = cashToDispense / 5;
        System.out.println("Sum of five bills : " + fiveBills);
    }

    public static void main(String[] args) {
        System.out.println("Most used character: " + findMostUsedCharacter("bbbbabababaaaaaaaaaaccc"));
        System.out.println("Most used character: " + findMostUsedCharacter("dsqdqsdqddddddddddddddd"));
        System.out.println();

        // Anagram
        System.out.println("Are marion & manoir anagrams ? " + isAnagram("marion", "manoir"));
        System.out.println("Are ironique & onirique anagrams ? " + isAnagram("ironique", "onirique"));
        System.out.println("Are sqdq & qaa anagrams ? " + isAnagram("sqdq", "qaa"));
        System.out.println();

        // Cash dispenser
        dispenseCash(185); // Should be 3 1 1 1
        System.out.println();
        dispenseCash(200); // Should be 4 0 0 0
        System.out.println();
    }
}
