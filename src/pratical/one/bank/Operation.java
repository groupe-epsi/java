package pratical.one.bank;

import java.util.Date;

public class Operation {
    private final Date date;
    private final int amount;
    private final OperationType type;

    public Operation(Date date, int amount, OperationType type) {
        this.date = date;
        this.amount = amount;
        this.type = type;
    }

    public Date getDate() {
        return date;
    }

    public int getAmount() {
        return amount;
    }

    public OperationType getType() {
        return type;
    }

    @Override
    public String toString() {
        return "Operation{" +
                "date=" + date +
                ", amount=" + amount +
                ", type=" + type +
                '}';
    }
}
