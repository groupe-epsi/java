package pratical.one.bank;

import pratical.one.bank.account.Account;
import pratical.one.bank.exceptions.AccountNotFoundException;

import java.util.HashMap;

public class Client {
    private static int nextAccountId = 0;
    private final int id;
    private String firstname;
    private final HashMap<Integer, Account> accounts;

    public Client(int id, String firstname) {
        this.id = id;
        this.firstname = firstname;
        this.accounts = new HashMap<>();
    }

    /**
     * Add an account to this client
     *
     * @param account The account to add
     */
    public int addAccount(Account account) {
        int accountId = nextAccountId++;
        this.accounts.put(accountId, account);
        return accountId;
    }

    /**
     * Retrieve an account from this client
     *
     * @param accountId The account id
     * @return The account associated with this id
     * @throws AccountNotFoundException This exception is thrown when no account with this id is found
     */
    public Account getAccount(int accountId) throws AccountNotFoundException {
        Account account = accounts.get(accountId);
        if (account == null) {
            throw new AccountNotFoundException("Account not found with id : " + accountId + " for client : " + id);
        }
        return account;
    }

    public int getId() {
        return id;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }
}
