package pratical.one.bank;

import pratical.one.bank.account.Account;
import pratical.one.bank.account.CompteCourant;
import pratical.one.bank.account.LivretA;
import pratical.one.bank.account.LivretJeune;
import pratical.one.bank.exceptions.AccountNotFoundException;
import pratical.one.bank.exceptions.ClientNotFoundException;
import pratical.one.bank.exceptions.OperationException;

import java.util.HashMap;
import java.util.Map;

public class Bank {
    private static int lastClientId = 0;

    private final Map<Integer, Client> clients;

    public Bank() {
        this.clients = new HashMap<>();
    }

    /**
     * Add a client to this bank
     *
     * @param firstname The firstname of the client
     * @return The id of the client
     */
    public int addClient(String firstname) {
        int id = lastClientId++;
        Client client = new Client(id, firstname);
        clients.put(id, client);
        return id;
    }

    /**
     * Retrieve a client from it's id
     *
     * @param clientId The client's id
     * @return The client
     * @throws ClientNotFoundException This exception is thrown when no client with this id is found
     */
    private Client getClient(int clientId) throws ClientNotFoundException {
        Client client = clients.get(clientId);
        if (client == null) {
            throw new ClientNotFoundException("Client not found with id: " + clientId);
        }
        return client;
    }

    /**
     * Add a compte courant to a client
     *
     * @param id               The client's id
     * @param allowedOverdraft The allowed overdraft
     */
    public int addCompteCourant(int id, int allowedOverdraft) {
        return clients.get(id).addAccount(new CompteCourant(allowedOverdraft));
    }

    /**
     * Add a livret A to a client
     *
     * @param id             The client's id
     * @param initialDeposit The initial deposit
     */
    public int addLivretA(int id, int initialDeposit) {
        return clients.get(id).addAccount(new LivretA(initialDeposit));
    }

    /**
     * Add a livret jeune to a client
     *
     * @param id             The client's id
     * @param initialDeposit The initial deposit
     */
    public int addLivretJeune(int id, int initialDeposit) {
        return clients.get(id).addAccount(new LivretJeune(initialDeposit));
    }

    /**
     * Deposit an amount of money using {@link Account deposit method}
     *
     * @param clientId  The client's id
     * @param accountId The account's id
     * @param amount    The amount of money to deposit
     * @return A deposit operation containing the date of the operation and the amount
     * @throws Exception - A {@link ClientNotFoundException} if no client with this id is found,
     *                   - A {@link AccountNotFoundException} if no account with this id is found,
     *                   - A {@link OperationException} if an error occurred with the operation
     */
    public Operation deposit(int clientId, int accountId, int amount) throws Exception {
        Client client = getClient(clientId);
        Account account = client.getAccount(accountId);
        return account.deposit(amount);
    }

    /**
     * Withdraw an amount of money using {@link Account deposit method}
     *
     * @param clientId  The client's id
     * @param accountId The account's id
     * @param amount    The amount of money to withdraw
     * @return A withdraw operation containing the date of the operation and the amount
     * @throws Exception - A {@link ClientNotFoundException} if no client with this id is found,
     *                   - A {@link AccountNotFoundException} if no account with this id is found,
     *                   - A {@link OperationException} if an error occurred with the operation
     */
    public Operation withdraw(int clientId, int accountId, int amount) throws Exception {
        Client client = getClient(clientId);
        Account account = client.getAccount(accountId);
        return account.withdraw(amount);
    }
}
