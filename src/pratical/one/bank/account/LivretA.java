package pratical.one.bank.account;

public class LivretA extends SavingBook {
    public final static int CEILING = 22_950;

    public LivretA(int initialDeposit) {
        super(CEILING, initialDeposit);
    }
}
