package pratical.one.bank.account;

import pratical.one.bank.Operation;
import pratical.one.bank.exceptions.CeilingException;
import pratical.one.bank.exceptions.OperationException;
import pratical.one.bank.exceptions.WithdrawalException;

public interface Account {

    /**
     * Deposit an amount of money
     *
     * @param amount The amount to deposit
     * @return A deposit operation containing the date of the operation and the amount
     * @throws OperationException A deposit might throw an exception depending of the type of account,
     *                            depositing more than the ceiling allows should throw
     *                            an {@link CeilingException} for example
     */
    Operation deposit(int amount) throws OperationException;

    /**
     * Withdraw an amount of money
     *
     * @param amount The amount to withdraw
     * @return A withdrawal operation containing the date of the operation and the amount
     * @throws OperationException A withdraw might throw an exception depending of the type of account,
     *                            withdrawing more than what overdraft allows should throw
     *                            an {@link WithdrawalException} for example
     */
    Operation withdraw(int amount) throws OperationException;
}
