package pratical.one.bank.account;

public class LivretJeune extends SavingBook {
    public final static int CEILING = 1_600;

    public LivretJeune(int initialDeposit) {
        super(CEILING, initialDeposit);
    }
}
