package pratical.one.bank.account;

import pratical.one.bank.exceptions.CeilingException;
import pratical.one.bank.Operation;
import pratical.one.bank.OperationType;
import pratical.one.bank.exceptions.WithdrawalException;

import java.util.Date;

public abstract class SavingBook implements Account {
    private final int ceilingAmount;
    private int money;

    public SavingBook(int ceilingAmount, int initialDeposit) {
        this.ceilingAmount = ceilingAmount;
        this.money = initialDeposit;
    }

    @Override
    public Operation deposit(int amount) throws CeilingException {
        if (money + amount > ceilingAmount) {
            throw new CeilingException("Ceiling amount exceeded");
        }
        this.money += amount;
        return new Operation(new Date(), amount, OperationType.DEPOSIT);
    }

    @Override
    public Operation withdraw(int amount) throws WithdrawalException {
        if (amount > money) {
            throw new WithdrawalException("No enough money in account");
        }
        this.money -= amount;
        return new Operation(new Date(), amount, OperationType.WITHDRAWAL);
    }

    @Override
    public String toString() {
        return "Livret{" +
                "ceilingAmount=" + ceilingAmount +
                ", money=" + money +
                '}';
    }
}
