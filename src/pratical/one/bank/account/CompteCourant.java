package pratical.one.bank.account;

import pratical.one.bank.Operation;
import pratical.one.bank.OperationType;
import pratical.one.bank.exceptions.WithdrawalException;

import java.util.Date;

public class CompteCourant implements Account {
    private final int allowedOverdraft;
    private int money;

    public CompteCourant(int allowedOverdraft) {
        this.money = 0;
        this.allowedOverdraft = allowedOverdraft;
    }

    @Override
    public Operation deposit(int amount) {
        this.money += amount;
        return new Operation(new Date(), amount, OperationType.DEPOSIT);
    }

    @Override
    public Operation withdraw(int amount) throws WithdrawalException {
        if (amount > this.money + this.allowedOverdraft) {
            throw new WithdrawalException("Overdraft exceeded");
        }
        this.money -= amount;
        return new Operation(new Date(), amount, OperationType.WITHDRAWAL);
    }

    @Override
    public String toString() {
        return "CompteCourant{" +
                "allowedOverdraft=" + allowedOverdraft +
                ", money=" + money +
                '}';
    }
}
