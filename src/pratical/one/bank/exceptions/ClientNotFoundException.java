package pratical.one.bank.exceptions;

public class ClientNotFoundException extends Exception {

    public ClientNotFoundException(String message) {
        super(message);
    }
}
