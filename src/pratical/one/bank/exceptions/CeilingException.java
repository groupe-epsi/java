package pratical.one.bank.exceptions;

public class CeilingException extends OperationException {

    public CeilingException(String message) {
        super(message);
    }
}
