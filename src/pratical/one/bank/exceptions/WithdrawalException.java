package pratical.one.bank.exceptions;

public class WithdrawalException extends OperationException {

    public WithdrawalException(String message) {
        super(message);
    }
}
