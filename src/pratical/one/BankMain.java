package pratical.one;

import pratical.one.bank.Bank;

public class BankMain {

    public static void main(String[] args) {
        Bank epsiBank = new Bank();

        int clientId = epsiBank.addClient("Armand");

        int compteCourantId = epsiBank.addCompteCourant(clientId, 100);
        int livretJeuneId = epsiBank.addLivretJeune(clientId, 300);

        try {
            System.out.println(epsiBank.withdraw(clientId, compteCourantId, 50));
        } catch (Exception e) {
            System.err.println("Transaction error occurred");
            e.printStackTrace();
        }

        System.out.println();

        try {
            System.out.println(epsiBank.deposit(clientId, livretJeuneId, 2000)); // Will throw an exception
        } catch (Exception e) {
            System.err.println("Transaction error occurred");
            e.printStackTrace();
        }
    }
}
